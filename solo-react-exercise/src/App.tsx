import React, { KeyboardEventHandler, useEffect, useState } from 'react';
import { Button, Col, Container, Dropdown, DropdownButton, FormControl, Row, Table } from 'react-bootstrap';
import DropdownItem from 'react-bootstrap/esm/DropdownItem';
import DropdownMenu from 'react-bootstrap/esm/DropdownMenu';
import DropdownToggle from 'react-bootstrap/esm/DropdownToggle';
import { getRepresentatives, getSenators } from './Services/RepresentativeService';
import './App.css';
import states from './Data/states';
import { FilterMenu, FilterToggle } from './Refs/filter-dropdown';
import IRepresentative from './Services/IRepresentative';

enum REPRESENTATIVE_SEARCH_TYPE {
  REPRESENTATIVE = "Representative",
  SENATOR = "Senator"
};

function App() {
  const [ representatives, setRepresentatives ] = useState([] as IRepresentative[]);
  const [ representativeSearch, setRepresentativeSearch ] = useState("");
  const [ representativeSearchType, setRepresentativeSearchType ] = useState("Representative Type");
  const [ selectedState, setSelectedState ] = useState("State");
  const [ selectedRepresentative, setSelectedRepresentative ] = useState(null as unknown as IRepresentative);
  const [ error, setError ] = useState(null as unknown as string);

  const filterDataWithSearch = (representatives: IRepresentative[]) => {
    return representatives.filter((representative) => {
      return representative.name.toLowerCase().indexOf(representativeSearch) > -1;
    });
  }

  const submitSearch = () => {
    if(representativeSearchType === "Representative Type" || representativeSearchType == null) {
      setError("Please select a representative type.");
      return;
    }
    if(selectedState === "State" || selectedState == null) {
      setError("Please select a state.");
      return;
    }     
    const state = selectedState;
    if(representativeSearchType === REPRESENTATIVE_SEARCH_TYPE.REPRESENTATIVE)
      getRepresentatives(state).then(data => setRepresentatives(filterDataWithSearch(data)));
    else if(representativeSearchType === REPRESENTATIVE_SEARCH_TYPE.SENATOR)
      getSenators(state).then(data => setRepresentatives(filterDataWithSearch(data)));
    setError(null as unknown as string);
  };

  const representativeSearchInput = 
    <FormControl 
      placeholder="Search for your representative"
      onKeyPress={(evt: React.KeyboardEvent<HTMLInputElement>) => {
        if(evt.key === 'Enter')
          submitSearch();
      }}
      onChange={(evt) => {
        setRepresentativeSearch(evt.target.value)
      }}
    />;

  const representativeSearchTypeDropdown = 
    <DropdownButton title={representativeSearchType}>
      <DropdownItem 
        onClick={() => setRepresentativeSearchType(REPRESENTATIVE_SEARCH_TYPE.REPRESENTATIVE)}>
        {REPRESENTATIVE_SEARCH_TYPE.REPRESENTATIVE}
      </DropdownItem>
      <DropdownItem 
        onClick={() => setRepresentativeSearchType(REPRESENTATIVE_SEARCH_TYPE.SENATOR)}>
        {REPRESENTATIVE_SEARCH_TYPE.SENATOR}
      </DropdownItem>
    </DropdownButton>;

    const stateDropdown =
      <Dropdown>
        <DropdownToggle className="dropdown-toggle btn btn-primary" as={FilterToggle}>
          {selectedState}
        </DropdownToggle>
        <DropdownMenu as={FilterMenu}>
          {Object.keys(states).map((state) => 
            <DropdownItem
              onClick={() => setSelectedState(state)}>
                {state}
            </DropdownItem>
          )}
        </DropdownMenu>
      </Dropdown>;

    const infoPanel = <div className="info-panel slightly-grey-text">
        {selectedRepresentative != null &&
          <>
            {selectedRepresentative.name.split(' ').reverse().map((firstOrLastName) => {
                return <div>{firstOrLastName}</div>
            })}
            <div>{selectedRepresentative.district}</div>
            <div>{selectedRepresentative.phone}</div>
            <div>{selectedRepresentative.office}</div>
            <a href={selectedRepresentative.link}>{selectedRepresentative.link}</a>
          </>
        }
      </div>;

    const representativeList = <Table striped bordered hover size="sm" className="representative-table">
      <thead>
        <tr className="slightly-grey-text">
          <td>Name</td>
          <td>Party</td>
        </tr>
      </thead>
      <tbody>
        {representatives.map((rep) => {
          return <tr className="representative-row" onClick={() => setSelectedRepresentative(rep)}>
            <td>{rep.name}</td>
            <td>{rep.party[0]}</td>
          </tr>;
        })}
      </tbody>
    </Table>;

  return (
    <div className="App">
      <h2 className="App-link">
        <b>
          Who's My Representative?
        </b>
      </h2>
      <Container>
        <Row className="justify-content-md-center">
          <Col>{representativeSearchInput}</Col>
          <Col md="2">{representativeSearchTypeDropdown}</Col>
          <Col xs lg="2">{stateDropdown}</Col>
          <Col><Button onClick={() => submitSearch()}>Submit</Button></Col>
        </Row>
      </Container>
      <div className="error-text">{error}</div>
      <div className="list-and-info-display">
        <div className="list-panel">
          <h3>List / <span className="App-link">{representativeSearchType}s</span></h3>
          {representativeList}
        </div>
        <div>       
          <h3>Info</h3>
          {infoPanel}
        </div>
      </div>
    </div>
  );
}

export default App;