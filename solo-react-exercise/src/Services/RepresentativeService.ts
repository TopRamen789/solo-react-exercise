import IRepresentative from "./IRepresentative";

const makeRequest = (url: string) => {
    return new Promise((resolve: (representatives: IRepresentative[]) => void, reject) => {
        let request = new XMLHttpRequest();
        request.open("GET", url);
        request.responseType = "json";
        request.onload = () => {
            console.log(request.response);
            if(request.status === 200) {
                resolve(request.response.results as IRepresentative[]);
            } else {
                reject("Not found.");
            }
        }
        request.send();
    });
};

const getRepresentatives = (state: string) => {
    const url = `http://localhost:3001/representatives/${state}`
    return makeRequest(url);
}

const getSenators = (state: string) => {
    const url = `http://localhost:3001/senators/${state}`
    return makeRequest(url);
}

export {getRepresentatives, getSenators};