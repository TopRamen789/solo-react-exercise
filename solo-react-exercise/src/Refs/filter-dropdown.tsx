import React, { Ref, useState } from "react";
import { FormControl } from "react-bootstrap";

interface IFilterToggleProps {
    children: React.ReactChildren;
    onClick: (e: React.MouseEvent<HTMLButtonElement>) => void;
    style?: React.CSSProperties;
    className?: string;
}

const FilterToggle = React.forwardRef((props: IFilterToggleProps, ref: Ref<HTMLButtonElement>) => (
    <button
        ref={ref}
        onClick={(e: React.MouseEvent<HTMLButtonElement>) => {
            e.preventDefault();
            props.onClick(e);
        }}
        style={props.style}
        className={props.className}
    >
        {props.children}
    </button>
));

interface IFilterMenuProps {
    children: React.ReactChildren;
    className?: string;
}

// forwardRef again here!
// Dropdown needs access to the DOM of the Menu to measure it
const FilterMenu = React.forwardRef((props: IFilterMenuProps, ref: Ref<HTMLButtonElement>) => {
    const [value, setValue] = useState('');

    return (
      <button
        ref={ref}
        className={props.className}
        style={{
            "position": "absolute",
            "backgroundColor": "white"
        }}
      >
        <FormControl
          autoFocus
          className="mx-3 my-2 w-auto"
          placeholder="Type to filter..."
          onChange={(e) => setValue(e.target.value)}
          value={value}
        />
        <ul className="list-unstyled">
            {(React.Children.toArray(props.children) as React.ReactChild[])
                .filter((child: React.ReactChild) => {
                    return !value || (child as React.ReactElement).props.children.toLowerCase().startsWith(value);
                })
            }
        </ul>
      </button>
    );
  },
);

export { FilterMenu, FilterToggle };